package com.zuhaibahmad.kodi;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = ".MainActivity";
    private static final String PATH = ".MainActivity.downloadPath";
    private ProgressDialog progressDialog;
    private String downloadPath;
    private String filePath;
    private String extractPath;

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context c, Intent intent) {
            String action = intent.getAction();

            if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {

                progressDialog.dismiss();

                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0));
                DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

                Cursor cursor = manager.query(query);
                if (cursor.moveToFirst()) {
                    if (cursor.getCount() > 0) {
                        int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                        if (status == DownloadManager.STATUS_SUCCESSFUL) {
                            String file = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));

                            if (file.contains(".zip")) {
                                progressDialog = ProgressDialog.show(MainActivity.this, "Extracting", "Please Wait While Extracting Zip File", true);

                                //Check if file exists
                                if(new File(file).exists()){
                                    filePath = new File(file).getAbsolutePath();
                                    //Save to shared prefs because the current file downloadPath will be removed as soon as the app closes
                                    getSharedPreferences("Kodi", MODE_PRIVATE).edit().putString(PATH,filePath).commit();
                                }

                                Log.e(TAG,"Extracting from: " + filePath);
                                Log.e(TAG,"Extracting in: " + downloadPath);

                                //Extract file to /Android/data/ directory
                                extractPath = Environment.getExternalStorageDirectory() + "/Android/data/";
                                new UnZipTask().execute(filePath, extractPath);

                            } else if (file.contains(".apk")) {

                                //Show install screen if it's apk
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setDataAndType(Uri.fromFile(new File(file)), "application/vnd.android.package-archive");
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        } else {
                            int message = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
                            Log.e(TAG, "Error: " + message);
                        }
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                Utils.appendLog(ex.toString());
                ex.printStackTrace();
            }
        });

        //Default file download location
        downloadPath = Environment.getExternalStorageDirectory() + File.separator + "Android" + File.separator
                + "data" + File.separator + getPackageName();

        findViewById(R.id.BTInstall).setOnClickListener(this);
        findViewById(R.id.BTUninstall).setOnClickListener(this);
        findViewById(R.id.BTSetup).setOnClickListener(this);
        findViewById(R.id.BTClean).setOnClickListener(this);
        findViewById(R.id.button).setOnClickListener(this);

        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.BTInstall) {
            downloadAPK();
        } else if (id == R.id.BTUninstall) {
            Uri packageURI = Uri.parse("package:" + Constants.packageName);
            Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
            startActivity(uninstallIntent);
        } else if (id == R.id.BTSetup) {
            downloadZip();
        } else if (id == R.id.BTClean) {
            deleteZip();
        }
    }

    private void deleteZip() {
        //If file downloadPath is empty, load from shared prefs
        if(filePath.isEmpty()){
            filePath = getSharedPreferences("Kodi", MODE_PRIVATE).getString(PATH, Environment.getExternalStorageDirectory() +
                    Constants.zipPath);
        }

        //Delete the file
        File file = new File(filePath);
        boolean deleted = file.delete();

        //Confirm deletion or error
        if (deleted) Toast.makeText(MainActivity.this, "Done!", Toast.LENGTH_SHORT).show();
        else Utils.showAlertDialogWithoutCancel(MainActivity.this, "Error!", "Error occurred while cleaning files");
    }

    private void downloadAPK() {
        progressDialog = ProgressDialog.show(MainActivity.this, "Downloading", "Please Wait While Downloading APK", true);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(Constants.apkURL));
        request.setDescription("Please wait while download is finished");
        request.setTitle("Downloading Kodi APK");

        // in order for this if to run, you must use the android 3.2 to compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }

        //Check for directory and create if does not exist
        File directory = new File(downloadPath);
        if (!directory.exists()) directory.mkdirs();

        Log.e(TAG,"Downloading Zip in: " + downloadPath);

        request.setDestinationInExternalPublicDir(downloadPath, "Kodi.apk");

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    private void downloadZip() {
        progressDialog = ProgressDialog.show(MainActivity.this, "Downloading", "Please Wait While Download Zip File. This May Take Upto 30 Minutes.", true);

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(Constants.zipURL));
        request.setDescription("Please wait while download is finished");
        request.setTitle("Downloading Kodi Data File");

        // in order for this if to run, you must use the android 3.2 to compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }

        //Check for directory and create if does not exist
        File directory = new File(downloadPath);
        if (!directory.exists()) directory.mkdirs();

        Log.e(TAG,"Downloading Zip in: " + downloadPath);

        //Set download location
        request.setDestinationInExternalPublicDir(downloadPath, Constants.zipName);

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
    }

    private class UnZipTask extends AsyncTask<String, Void, Boolean> {
        @SuppressWarnings("rawtypes")
        @Override
        protected Boolean doInBackground(String... params) {
            String filePath = params[0];
            String destinationPath = params[1];

            File archive = new File(filePath);
            try {
                ZipFile zipfile = new ZipFile(archive);
                for (Enumeration e = zipfile.entries(); e.hasMoreElements(); ) {
                    ZipEntry entry = (ZipEntry) e.nextElement();
                    unzipEntry(zipfile, entry, destinationPath);
                }

                UnzipUtil d = new UnzipUtil(filePath, destinationPath);
                d.unzip();

            } catch (Exception e) {
                Log.e(TAG, "Error!", e);
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            deleteZip();
            progressDialog.dismiss();
        }

        private void unzipEntry(ZipFile zipfile, ZipEntry entry, String outputDir) throws IOException {

            BufferedInputStream inputStream = null;
            BufferedOutputStream outputStream = null;

            try {
                if (entry.isDirectory()) {
                    createDir(new File(outputDir, entry.getName()));
                    return;
                }

                File outputFile = new File(outputDir, entry.getName());
                if (!outputFile.getParentFile().exists())
                    createDir(outputFile.getParentFile());

                inputStream = new BufferedInputStream(zipfile.getInputStream(entry));
                outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));

            } catch (Exception e) {
                progressDialog.dismiss();
                Log.e(TAG, "Error Occurred While Extracting Zip File", e);
            } finally {
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
                if (inputStream != null)
                    inputStream.close();
            }
        }

        private void createDir(File dir) {
            if (dir.exists())
                return;
            if (!dir.mkdirs())
                throw new RuntimeException("Can not create dir " + dir);
        }
    }

    private class UnzipUtil {
        private String zipFile;
        private String location;

        public UnzipUtil(String zipFile, String location) {
            this.zipFile = zipFile;
            this.location = location;
            dirChecker("");
        }

        public void unzip() {
            try {
                FileInputStream fin = new FileInputStream(zipFile);
                ZipInputStream zin = new ZipInputStream(fin);
                ZipEntry ze = null;
                while ((ze = zin.getNextEntry()) != null) {
                    Log.v("Decompress", "Unzipping " + ze.getName());
                    final ZipEntry finalZe = ze;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.setMessage("Unzipping " + finalZe.getName());
                        }
                    });
                    if (ze.isDirectory()) {
                        dirChecker(ze.getName());
                    } else {
                        FileOutputStream fout = new FileOutputStream(location + ze.getName());
                        byte[] buffer = new byte[8192];
                        int len;
                        while ((len = zin.read(buffer)) != -1) fout.write(buffer, 0, len);
                        fout.close();
                        zin.closeEntry();
                    }
                }
                zin.close();
            } catch (Exception e) {
                Log.e("Decompress", "unzip", e);
            }
        }

        private void dirChecker(String dir) {
            File f = new File(location + dir);
            if (!f.isDirectory()) f.mkdirs();
        }
    }
}