package com.zuhaibahmad.kodi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private View mProgressView;
    private EditText usernameEditText, passwordEditText;
    private Button loginButton;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().hide();

        // Set up the login form.
        mProgressView = findViewById(R.id.login_progress);
        usernameEditText = (EditText) findViewById(R.id.ETUsername);
        passwordEditText = (EditText) findViewById(R.id.ETPassword);
        loginButton = (Button) findViewById(R.id.BLogIn);
        //Load Username from prefs
        String username = getSharedPreferences("AppPrefs", Context.MODE_PRIVATE).getString(Constants.EXTRA_USERNAME, "");
        usernameEditText.setText(username);
        String password = getSharedPreferences("AppPrefs", Context.MODE_PRIVATE).getString(Constants.EXTRA_PASSWORD, "");
        passwordEditText.setText(password);
        // On Click listeners
        usernameEditText.setOnClickListener(this);
        passwordEditText.setOnClickListener(this);
        loginButton.setOnClickListener(this);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin(final String username, final String password) {
        String URL = Constants.loginURL + username;
        URL.trim();

        if (Utils.isNetworkOnline(this)) {
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            showProgress(false);
                            int success;
                            try {
                                success = jsonObject.getInt("success");
                                if(success == 1){
                                    int status = jsonObject.getJSONObject("user").getInt("status");
                                    if(status == 1) {
                                        int userID = jsonObject.getJSONObject("user").getInt("user_id");
                                        checkPassword(userID, username, password);
                                    }else{
                                        Utils.showAlertDialogWithoutCancel(LoginActivity.this,"Account Expired!", "Please visit www.loadmyandroid.com to renew your subscription.");
                                    }
                                }else{
                                    Utils.showAlertDialogWithoutCancel(LoginActivity.this,"Error!", "Invalid Username or Password");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showAlertDialogWithoutCancel(LoginActivity.this, "Error!", "Error Occurred While Logging In...");
                        }
                    });
            queue = Volley.newRequestQueue(this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showAlertDialogWithoutCancel(this, "Error!", "Network Not Found!");
        }
    }

    public void checkPassword(final int user_id, final String username, final String password) {
        String URL = Constants.passwordCheckURL + user_id + "/" + password;
        URL.trim();

        if (Utils.isNetworkOnline(this)) {
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {
                            showProgress(false);
                            int success;
                            try {
                                success = jsonObject.getInt("success");
                                if(success == 1){
                                    SharedPreferences.Editor editor = getSharedPreferences("AppPrefs", MODE_PRIVATE).edit();
                                    editor.putString(Constants.EXTRA_USERNAME,username);
                                    editor.putString(Constants.EXTRA_PASSWORD,password);
                                    editor.commit();
                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                    Toast.makeText(LoginActivity.this, "Login Successful!", Toast.LENGTH_SHORT).show();
                                    LoginActivity.this.finish();
                                }else{
                                    Utils.showAlertDialogWithoutCancel(LoginActivity.this,"Error!", "Invalid Username or Password");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showAlertDialogWithoutCancel(LoginActivity.this, "Error!", "Error Occurred While Logging In...");
                        }
                    });
            queue = Volley.newRequestQueue(this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showAlertDialogWithoutCancel(this, "Error!", "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.BLogIn) {
            showProgress(true);
            String username = usernameEditText.getText().toString();
            String password = passwordEditText.getText().toString();
            if (username.isEmpty()) {
                usernameEditText.setError("Field Can't Be Left Empty");
                showProgress(false);
                return;
            }
            if (password.isEmpty()) {
                passwordEditText.setError("Field Can't Be Left Empty");
                showProgress(false);
                return;
            }
            attemptLogin(username, password);
        }
    }
}
